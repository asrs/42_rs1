#!/bin/sh -e

WORDPRESS_PASSWD=$(openssl rand -base64 32 | head -c 32)

mysql_install()
{
  sudo zypper -n in mariadb
  sudo systemctl enable mysql
  sudo systemctl start mysql

  cat<<EOF | sudo mysql -u root
create database wordpress;
grant all privileges on wordpress.* to wordpress@localhost
  identified by '${WORDPRESS_PASSWD}';
flush privileges;
exit
EOF
}

wordpress_install()
{
  O=http://download.opensuse.org
  A=${O}/repositories/server:/php:/applications/openSUSE_Leap_15.0/

  sudo zypper ar -f -n Applications ${A} Applications
  sudo zypper -n --gpg-auto-import-keys up
  sudo zypper -n in wordpress

  cat <<EOF | sudo tee /etc/wordpress/wp-config.php
<?php
define('DB_NAME', 'wordpress');
define('DB_USER', 'wordpress');
define('DB_PASSWORD', '${WORDPRESS_PASSWD}');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('AUTH_KEY', '$(openssl rand -base64 64 | head -c 64)');
define('SECURE_AUTH_KEY', '$(openssl rand -base64 64 | head -c 64)');
define('LOGGED_IN_KEY', '$(openssl rand -base64 64 | head -c 64)');
define('NONCE_KEY', '$(openssl rand -base64 64 | head -c 64)');
define('AUTH_SALT', '$(openssl rand -base64 64 | head -c 64)');
define('SECURE_AUTH_SALT', '$(openssl rand -base64 64 | head -c 64)');
define('LOGGED_IN_SALT', '$(openssl rand -base64 64 | head -c 64)');
define('NONCE_SALT', '$(openssl rand -base64 64 | head -c 64)');
\$table_prefix  = 'wp_';
define('WP_CONTENT_DIR', '/srv/www/wordpress/wp-content');
define('DISALLOW_FILE_MODS', false);
define('AUTOMATIC_UPDATER_DISABLED', false);
define('WP_DEBUG', false);
\$_SERVER['HTTPS'] = 'on';
define('FS_METHOD', 'direct');
if ( !defined('ABSPATH') )
  define('ABSPATH', '/srv/www/wordpress');
require_once(ABSPATH . 'wp-settings.php');
?>
EOF
}

apache_install()
{
  sudo zypper -n in apache2
  sudo systemctl enable apache2
  sudo gensslcert

  cat <<EOF | sudo tee /etc/apache2/conf.d/wordpress.conf
<VirtualHost _default_:443>
  SSLEngine on
  SSLCertificateFile /etc/apache2/ssl.crt/$(hostname -f)-server.crt
  SSLCertificateKeyFile /etc/apache2/ssl.key/$(hostname -f)-server.key

  ServerName $(hostname -f)
  DocumentRoot /srv/www/wordpress
  DirectoryIndex index.php index.html
  ErrorLog /var/log/apache2/wp-error.log
  TransferLog /var/log/apache2/wp-access.log
  Alias /wp-content /srv/www/wordpress/wp-content

  <Directory /srv/www/wordpress>
    Options FollowSymLinks
    Require all granted
  </Directory>

  <Directory /srv/www/wordpress/wp-content>
    Options FollowSymLinks
    Require all granted
  </Directory>
</VirtualHost>
EOF

  sudo firewall-cmd --add-service=https --permanent
  sudo firewall-cmd --reload

  sudo a2enflag SSL
  sudo a2enmod ssl
  sudo a2enmod php7
  sudo systemctl enable apache2
  sudo systemctl restart apache2
}

wordpress_main()
{
  mysql_install
  wordpress_install
  apache_install
}

wordpress_main
